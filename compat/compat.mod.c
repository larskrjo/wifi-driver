#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xa8a0d3ac, "module_layout" },
	{ 0x60a13e90, "rcu_barrier" },
	{ 0x3fec048f, "sg_next" },
	{ 0xded46080, "pci_bus_write_config_word" },
	{ 0x7d11c268, "jiffies" },
	{ 0x35b6b772, "param_ops_charp" },
	{ 0xd5f2172f, "del_timer_sync" },
	{ 0x27e1a049, "printk" },
	{ 0xb4390f9a, "mcount" },
	{ 0x8c55a07f, "pci_bus_write_config_dword" },
	{ 0x637d3b59, "pci_find_capability" },
	{ 0x9c491f60, "sg_alloc_table" },
	{ 0x76011256, "pci_bus_read_config_word" },
	{ 0x1dc6ced3, "pci_bus_read_config_dword" },
	{ 0xe52947e7, "__phys_addr" },
	{ 0x74c134b9, "__sw_hweight32" },
	{ 0x4cbbd171, "__bitmap_weight" },
	{ 0x57c4691b, "queue_delayed_work" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "F9680197EEA1CDE95D96EDA");
