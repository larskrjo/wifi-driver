/*
 * Copyright (C) 2013 Lars Kristian Johansen <larskrjo@mit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

/**
 * Static rate control algorithm - uses values in DebugFS to control rate
 *
 * Supports the following:
 * 802.11n
 * -- 0-31 MCS rates - tunable runtime via DebugFS
 * -- 40Mhz channels - tunable runtime via DebugFS
 * -- Short guard intervals - tunable runtime via DebugFS
 * -- Space Time Block Coding (STBC) - static
 * -- Low Density Parity Check (LDPC) - static
 * 802.11g
 * -- 0-11 Legacy Rates - tunable runtime via
 *
 * NB! Must compile with either legacy or non-legacy mode by setting USE_LEGACY.
 */
#include <linux/netdevice.h>
#include <linux/types.h>
#include <linux/skbuff.h>
#include <linux/debugfs.h>
#include <linux/random.h>
#include <linux/ieee80211.h>
#include <net/mac80211.h>
#include "rate.h"
#include "rc80211_lars_ht.h"

/* Define index to a MCS rate. */
#define GROUP_IDX(_streams, _sgi, _ht40)	\
	LARS_MAX_STREAMS * 2 * _ht40 +	\
	LARS_MAX_STREAMS * _sgi +		\
	_streams - 1

/* MCS rate information for an MCS group. */
#define MCS_GROUP(_streams, _sgi, _ht40)				\
	[GROUP_IDX(_streams, _sgi, _ht40)] = {				\
	.streams = _streams,						\
	.flags =							\
		(_sgi ? IEEE80211_TX_RC_SHORT_GI : 0) |			\
		(_ht40 ? IEEE80211_TX_RC_40_MHZ_WIDTH : 0)		\
}

/**
 * MCS rate groups based on streams, ht_40 and sg.
 */
const struct mcs_group lars_mcs_groups[] = {
	MCS_GROUP(1, 0, 0),
	MCS_GROUP(2, 0, 0),
	MCS_GROUP(3, 0, 0),
	MCS_GROUP(4, 0, 0),
	MCS_GROUP(1, 1, 0),
	MCS_GROUP(2, 1, 0),
	MCS_GROUP(3, 1, 0),
	MCS_GROUP(4, 1, 0),
	MCS_GROUP(1, 0, 1),
	MCS_GROUP(2, 0, 1),
	MCS_GROUP(3, 0, 1),
	MCS_GROUP(4, 0, 1),
	MCS_GROUP(1, 1, 1),
	MCS_GROUP(2, 1, 1),
	MCS_GROUP(3, 1, 1),
	MCS_GROUP(4, 1, 1),
};

/**
 * Called right after a packet is sent, contains info regarding the packet transmission.
 */
static void lars_ht_tx_status(void *priv, struct ieee80211_supported_band *sband,
                      struct ieee80211_sta *sta, void *priv_sta,
                      struct sk_buff *skb)
{
	struct lars_ht_sta_priv *lsp = priv_sta;
	struct ieee80211_tx_info *info = IEEE80211_SKB_CB(skb);
#ifdef CPTCFG_MAC80211_DEBUGFS
	// Do nothing but log event.
	rate_control_lars_event_tx_status(&lsp->events, info);
#endif
}

/**
 * Called right before a packet is sent, sets the rate for transmission.
 */
static void lars_ht_get_rate(void *priv, struct ieee80211_sta *sta, void *priv_sta,
                     struct ieee80211_tx_rate_control *txrc)
{
	const struct mcs_group *group;
	struct ieee80211_tx_info *info = IEEE80211_SKB_CB(txrc->skb);
	struct ieee80211_tx_rate *rate = &info->control.rates[0];
	struct ieee80211_supported_band *sband = txrc->sband;
	struct lars_ht_sta_priv *lsp = priv_sta;
	struct lars_ht_sta *lhts = &lsp->ht;
	struct lars_sta *lls = &lsp->legacy;
	struct lars_ht_priv *lp = priv;
	int index;
	int streams;
	int sg;
	int ht_40;
	// Set transmission limit -> use retry value set by iwconfig
	rate->count = txrc->hw->conf.short_frame_max_tx_count;

	/**
	 * If you are not sending broadcast and do not want to control the rate of broadcast and management frames.
	 */
	if(!CONTROL_BROADCAST) {
		// For no-ack and management frames, send with lowest rate, else continue.
		if (rate_control_send_low(sta, priv_sta, txrc))
			return;
	}
	else {
		/**
		 * NOTE: Assumes HT capable!
		 */
		// INDEX TO BE USED FOR BOTH BROADCAST AND UNICAST
		index = rate->count==8?0:rate->count;
		// For no-ack and management frames.
		if (rate_control_send_low(sta, priv_sta, txrc)) {
				streams = 1;
				ht_40 = 0;
				sg = 0;

				group = &lars_mcs_groups[GROUP_IDX(streams, sg, ht_40)];
				rate->count = 1;
				rate->idx = index;
				rate->flags = IEEE80211_TX_RC_MCS | group->flags;
			return;
		}
	}

	if (!lsp->is_ht)
		goto use_legacy;

	info->flags |= lhts->tx_flags;

	/**
	 * Extract values from DebugFS
	 */
//	index = lp->mcs_index;
	streams = 1 + (index / MCS_GROUP_RATES);
	ht_40 = lp->mcs_ht40;
	sg = lp->mcs_sg;
	/**
	 * Make sure mcs rate is valid, else set to lowest rate
	 */
	if(index >= MCS_GROUP_RATES * LARS_MAX_STREAMS || index < 0)
		index = 0;
	if(streams > LARS_MAX_STREAMS || streams < 1)
		streams = 1;

	// If not supported, return
	if(!lhts->groups[GROUP_IDX(streams, sg, ht_40)].supported)
		return;
	// Set current rate to values from DebugFS
	group = &lars_mcs_groups[GROUP_IDX(streams, sg, ht_40)];
	rate->idx = index;
	rate->flags = IEEE80211_TX_RC_MCS | group->flags;
#ifdef CPTCFG_MAC80211_DEBUGFS
	rate_control_lars_event_tx_rate(&lsp->events,
			rate->idx, -1);
#endif
	return;

use_legacy:
	index = lp->legacy_index;
	if(index < 0)
		rate->idx = lls->r[0].rix;
	else if(index >= lls->n_rates)
		rate->idx = lls->r[lls->n_rates-1].rix;
	else
		rate->idx = lls->r[index].rix;

#ifdef CPTCFG_MAC80211_DEBUGFS
	rate_control_lars_event_tx_rate(&lsp->events,
			lls->r[index].rix, sband->bitrates[lls->r[index].rix].bitrate);
#endif
}

/**
 * Called while a station is connecting, adjust parameters for the station.
 */
static void lars_ht_rate_init(void *priv, struct ieee80211_supported_band *sband,
                      struct ieee80211_sta *sta, void *priv_sta)
{
	struct lars_ht_priv *lp = priv;
	struct lars_ht_sta_priv *lsp = priv_sta;
	struct lars_ht_sta *lhts = &lsp->ht;
	struct ieee80211_mcs_info *mcs = &sta->ht_cap.mcs;
	struct lars_sta *lls;
	unsigned int n;
	u16 sta_cap = sta->ht_cap.cap;
	int n_supported = 0;
	int stbc;
	int i;

	/* fall back to the legacy mode for legacy stations */
	if (lp->use_legacy || !sta->ht_cap.ht_supported)
		goto use_legacy;

	lsp->is_ht = true;
	memset(lhts, 0, sizeof(*lhts));

	lhts->sta = sta;

	/**
	 * Does the station have STBC capability? If so, use it.
	 */
	stbc = (sta_cap & IEEE80211_HT_CAP_RX_STBC) >>
		IEEE80211_HT_CAP_RX_STBC_SHIFT;
	lhts->tx_flags |= stbc << IEEE80211_TX_CTL_STBC_SHIFT;
	/**
	 * Does the station have LDPC capability? If so, use it.
	 */
	if (sta_cap & IEEE80211_HT_CAP_LDPC_CODING)
		lhts->tx_flags |= IEEE80211_TX_CTL_LDPC;
	/**
	 * Since we are using HT capabilities, it supports AMPDUs
	 */
	lhts->tx_flags |= IEEE80211_TX_CTL_AMPDU;

	for (i = 0; i < ARRAY_SIZE(lhts->groups); i++) {
		lhts->groups[i].supported = 0;

		if (lars_mcs_groups[i].flags & IEEE80211_TX_RC_SHORT_GI) {
			if (lars_mcs_groups[i].flags & IEEE80211_TX_RC_40_MHZ_WIDTH) {
				if (!(sta_cap & IEEE80211_HT_CAP_SGI_40))
					continue;
			} else {
				if (!(sta_cap & IEEE80211_HT_CAP_SGI_20))
					continue;
			}
		}
		/* If station does not support 40Mhz band, but this group uses it, skip it. */
		if (lars_mcs_groups[i].flags & IEEE80211_TX_RC_40_MHZ_WIDTH &&
		    sta->bandwidth < IEEE80211_STA_RX_BW_40)
			continue;

		/* Mark MCS > 7 as unsupported if STA is in static SMPS mode. */
		if (sta->smps_mode == IEEE80211_SMPS_STATIC &&
		    lars_mcs_groups[i].streams > 1)
			continue;

		/**
		 * Set supported bitmask
		 */
		lhts->groups[i].supported =
			mcs->rx_mask[lars_mcs_groups[i].streams - 1];

		if (lhts->groups[i].supported)
			n_supported++;
	}

	if (!n_supported)
		goto use_legacy;

	return;

use_legacy:
	lsp->is_ht = false;
	memset(&lsp->legacy, 0, sizeof(lsp->legacy));
	lsp->legacy.r = lsp->ratelist;
	lls = &lsp->legacy;
	n = 0;
	lls->sta = sta;
	/**
	 * Count supported bitrates.
	 */
	for (i = 0; i < sband->n_bitrates; i++) {
		struct lars_rate *lr = &lls->r[n];
		if (!rate_supported(sta, sband->band, i))
			continue;
		n++;
		memset(lr, 0, sizeof(*lr));
		lr->rix = i;
		lr->bitrate = sband->bitrates[i].bitrate / 5;
	}
	/**
	 * Mark remaining bitrates as unavailable.
	 */
	for (i = n; i < sband->n_bitrates; i++) {
		struct lars_rate *lr = &lls->r[i];
		lr->rix = -1;
	}
	lls->n_rates = n;
}

static void *lars_ht_alloc_sta(void *priv, struct ieee80211_sta *sta, gfp_t gfp)
{
	struct ieee80211_supported_band *sband;
	struct lars_ht_sta_priv *lsp;
	struct lars_ht_priv *lp = priv;
	struct ieee80211_hw *hw = lp->hw;
	int max_rates = 0;
	int i;

	for (i = 0; i < IEEE80211_NUM_BANDS; i++) {
		sband = hw->wiphy->bands[i];
		if (sband && sband->n_bitrates > max_rates)
			max_rates = sband->n_bitrates;
	}

	lsp = kzalloc(sizeof(*lsp), gfp);
	if (!lsp)
		return NULL;

	lsp->ratelist = kzalloc(sizeof(struct lars_rate) * max_rates, gfp);
	if (!lsp->ratelist)
		goto error;

#ifdef CPTCFG_MAC80211_DEBUGFS
	spin_lock_init(&lsp->events.lock);
	init_waitqueue_head(&lsp->events.waitqueue);
#endif

	return lsp;

error:
	kfree(lsp);
	return NULL;
}

static void lars_ht_free_sta(void *priv, struct ieee80211_sta *sta, void *priv_sta)
{
	struct lars_ht_sta_priv *lsp = priv_sta;
	kfree(lsp->ratelist);
	kfree(lsp);
}

static void *lars_ht_alloc(struct ieee80211_hw *hw, struct dentry *debugfsdir)
{
	struct lars_ht_priv *lp;
#ifdef CPTCFG_MAC80211_DEBUGFS
	struct rc_lars_debugfs_entries *de;
#endif

	lp = kzalloc(sizeof(struct lars_ht_priv), GFP_ATOMIC);
	if (!lp)
		return NULL;

	lp->hw = hw;
	// Set default parameters
	lp->use_legacy = USE_LEGACY;
	lp->legacy_index = DEFAULT_LEGACY_RATE_INDEX;
	lp->mcs_index = DEFAULT_MCS_RATE_INDEX;
	lp->mcs_ht40 = DEFAULT_HT_40;
	lp->mcs_sg = DEFAULT_SG;

#ifdef CPTCFG_MAC80211_DEBUGFS
	de = &lp->dentries;
	de->legacy_index = debugfs_create_u32("legacy_index", S_IRUSR | S_IWUSR,
					debugfsdir, &lp->legacy_index);
	de->mcs_index = debugfs_create_u32("mcs_index", S_IRUSR | S_IWUSR,
					debugfsdir, &lp->mcs_index);
	de->mcs_ht40 = debugfs_create_u32("mcs_ht40", S_IRUSR | S_IWUSR,
					debugfsdir, &lp->mcs_ht40);
	de->mcs_sg = debugfs_create_u32("mcs_sg", S_IRUSR | S_IWUSR,
					debugfsdir, &lp->mcs_sg);
	de->use_legacy = debugfs_create_u32("use_legacy", S_IRUSR,
					debugfsdir, &lp->use_legacy);
#endif

	return lp;
}

static void lars_ht_free(void *priv)
{
	struct lars_ht_priv *lp = priv;
#ifdef CPTCFG_MAC80211_DEBUGFS
	struct rc_lars_debugfs_entries *de = &lp->dentries;
	debugfs_remove(de->legacy_index);
	debugfs_remove(de->mcs_index);
	debugfs_remove(de->mcs_ht40);
	debugfs_remove(de->mcs_sg);
	debugfs_remove(de->use_legacy);
#endif
	kfree(lp);
}

static struct rate_control_ops mac80211_lars_ht = {
	.name = "lars_ht",
	.tx_status = lars_ht_tx_status,
	.get_rate = lars_ht_get_rate,
	.rate_init = lars_ht_rate_init,
	.alloc_sta = lars_ht_alloc_sta,
	.free_sta = lars_ht_free_sta,
	.alloc = lars_ht_alloc,
	.free = lars_ht_free,
#ifdef CPTCFG_MAC80211_DEBUGFS
	.add_sta_debugfs = lars_ht_add_sta_debugfs,
	.remove_sta_debugfs = lars_ht_remove_sta_debugfs,
#endif
};

int __init rc80211_lars_ht_init(void)
{
	return ieee80211_rate_control_register(&mac80211_lars_ht);
}

void rc80211_lars_ht_exit(void)
{
	ieee80211_rate_control_unregister(&mac80211_lars_ht);
}
