/*
 * Copyright (C) 2013 Lars Kristian Johansen <larskrjo@mit.edu>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#ifndef __RC_LARS_HT_H
#define __RC_LARS_HT_H

/**
 * Set this if you want to use legacy rates, else HT rates are used.
 */
#define CONTROL_BROADCAST 1

/**
 * Set this if you want to use legacy rates, else HT rates are used.
 */
#define USE_LEGACY 0
/**
 * Tunable parameters, can be changed in runtime by DebugFS
 */
#define DEFAULT_LEGACY_RATE_INDEX 0
#define DEFAULT_MCS_RATE_INDEX 0
#define DEFAULT_HT_40 0
#define DEFAULT_SG 0
/*
 * Do not change the following:
 */
#define LARS_MAX_STREAMS	4
#define LARS_STREAM_GROUPS	4
#define MCS_GROUP_RATES	8
/**
 * DebugFS Events
 */
enum rc_lars_event_type {
	RC_LARS_EVENT_TYPE_TX_STATUS,
	RC_LARS_EVENT_TYPE_TX_RATE,
};

union rc_lars_event_data {
	/* RC_LARS_EVENT_TX_STATUS */
	struct {
		u32 flags;
		struct ieee80211_tx_info tx_status;
	};
	/* RC_LARS_EVENT_TYPE_TX_RATE */
	struct {
		int index;
		int rate;
	};
};

struct rc_lars_event {
	/* The time when the event occurred */
	unsigned long timestamp;
	/* Event ID number */
	unsigned int id;
	/* Type of event */
	enum rc_lars_event_type type;
	/* type specific data */
	union rc_lars_event_data data;
};

/* Size of the event ring buffer. */
#define RC_LARS_EVENT_RING_SIZE 32

struct rc_lars_event_buffer {
	/* Counter that generates event IDs */
	unsigned int ev_count;
	/* Ring buffer of events */
	struct rc_lars_event ring[RC_LARS_EVENT_RING_SIZE];
	/* Index to the entry in events_buf to be reused */
	unsigned int next_entry;
	/* Lock that guards against concurrent access to this buffer struct */
	spinlock_t lock;
	/* Wait queue for poll/select and blocking I/O */
	wait_queue_head_t waitqueue;
};

struct rc_lars_events_file_info {
	/* The event buffer we read */
	struct rc_lars_event_buffer *events;
	/* The entry we have should read next */
	unsigned int next_entry;
};

/**
 * struct rc_lars_debugfs_entries - tunable parameters
 *
 * Algorithm parameters, tunable via debugfs.
 * @mcs_index: describes which mcs rate index is used for tx
 */
struct rc_lars_debugfs_entries {
	struct dentry *use_legacy;
	struct dentry *mcs_index;
	struct dentry *legacy_index;
	struct dentry *mcs_ht40;
	struct dentry *mcs_sg;
};

void rate_control_lars_event_tx_status(struct rc_lars_event_buffer *buf,
				      struct ieee80211_tx_info *stat);

void rate_control_lars_event_rate_change(struct rc_lars_event_buffer *buf,
					       int index, int rate);

void rate_control_lars_event_tx_rate(struct rc_lars_event_buffer *buf,
					   int index, int rate);

void rate_control_lars_event_pf_sample(struct rc_lars_event_buffer *buf,
					     s32 pf_sample, s32 prop_err,
					     s32 int_err, s32 der_err);

void rate_control_lars_add_sta_debugfs(void *priv, void *priv_sta,
					     struct dentry *dir);

void rate_control_lars_remove_sta_debugfs(void *priv, void *priv_sta);


struct mcs_group {
	u32 flags;
	unsigned int streams;
};

extern const struct mcs_group lars_mcs_groups[];

struct lars_mcs_group_data {
	/**
	 *  bitfield of supported MCS rates of this group
	 *  1111111111 means all rates are supported in this group
	 *  0000000000 means no rates are supported in this group
	 */
	u8 supported;
};

/**
 * 802.11n station
 */
struct lars_ht_sta {
	// 802.11n station
	struct ieee80211_sta *sta;
	// TX flags to add for frames for this sta
	u32 tx_flags;
	// MCS rate groups
	struct lars_mcs_group_data groups[LARS_MAX_STREAMS * LARS_STREAM_GROUPS];
};

struct lars_rate {
	int bitrate;
	int rix;
};

/**
 * 802.11g station
 */
struct lars_sta {
	// 802.11g station
	struct ieee80211_sta *sta;
	// Number of legacy rates
	int n_rates;
	// Legacy rates
	struct lars_rate *r;
};

struct lars_ht_sta_priv {
	union {
		struct lars_ht_sta ht;
		struct lars_sta legacy;
	};
	void *ratelist;
	bool is_ht;
#ifdef CPTCFG_MAC80211_DEBUGFS
	// Event buffer
	struct rc_lars_event_buffer events;
	// Events debugfs file entry
	struct dentry *events_entry;
#endif
};

struct lars_ht_priv {
	struct ieee80211_hw *hw;
	int use_legacy;
	int legacy_index;
	int mcs_index;
	int mcs_ht40;
	int mcs_sg;

#ifdef CPTCFG_MAC80211_DEBUGFS
	// Debugfs entries created for the parameters above.
	struct rc_lars_debugfs_entries dentries;
#endif
};

void lars_ht_add_sta_debugfs(void *priv, void *priv_sta, struct dentry *dir);
void lars_ht_remove_sta_debugfs(void *priv, void *priv_sta);

#endif
