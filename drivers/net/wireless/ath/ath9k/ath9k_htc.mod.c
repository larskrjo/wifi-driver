#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xa8a0d3ac, "module_layout" },
	{ 0xfea47e67, "mac80211_ieee80211_rx" },
	{ 0x74d139db, "ath9k_hw_set_txq_props" },
	{ 0x3590d3d6, "ath9k_hw_init" },
	{ 0x92295a0d, "kmalloc_caches" },
	{ 0x44e935f8, "ath9k_hw_deinit" },
	{ 0x7819f896, "ath9k_hw_cfg_output" },
	{ 0xb43a926b, "backport_dependency_symbol" },
	{ 0xadaabe1b, "pv_lock_ops" },
	{ 0x15692c87, "param_ops_int" },
	{ 0x339f225b, "device_release_driver" },
	{ 0x60a13e90, "rcu_barrier" },
	{ 0x35d0b3e4, "ath9k_hw_set_gpio" },
	{ 0x15056f21, "ath9k_cmn_init_crypto" },
	{ 0xc53fa74e, "ieee80211_queue_work" },
	{ 0x798d2759, "dev_set_drvdata" },
	{ 0xab7aaa0d, "led_classdev_register" },
	{ 0x4e4f3c69, "ath9k_hw_btcoex_enable" },
	{ 0xb0472105, "ath9k_hw_wait" },
	{ 0x9e65e8d6, "ath9k_cmn_get_hw_crypto_keytype" },
	{ 0x91fddf93, "ath9k_hw_stopdmarecv" },
	{ 0x67973081, "ath_key_delete" },
	{ 0x251e5cff, "ath9k_cmn_update_txpow" },
	{ 0x1637ff0f, "_raw_spin_lock_bh" },
	{ 0x7ef39823, "ieee80211_hdrlen" },
	{ 0xd271450d, "ieee80211_beacon_get_tim" },
	{ 0xeeae78c8, "ath9k_hw_gpio_get" },
	{ 0xf527c3c6, "ath_regd_init" },
	{ 0x88bfa7e, "cancel_work_sync" },
	{ 0x6e72d5f7, "usb_kill_urb" },
	{ 0x448eac3e, "kmemdup" },
	{ 0x68a40049, "ath9k_cmn_get_curchannel" },
	{ 0x86653495, "ieee80211_unregister_hw" },
	{ 0xeae3dfd6, "__const_udelay" },
	{ 0xfb0e29f, "init_timer_key" },
	{ 0xfb5f846a, "cancel_delayed_work_sync" },
	{ 0x28f2a373, "mutex_unlock" },
	{ 0xbf9ef32c, "ieee80211_iterate_active_interfaces_atomic" },
	{ 0xa113d6ab, "ath9k_hw_setrxfilter" },
	{ 0x2fb502c7, "ath9k_hw_get_txq_props" },
	{ 0x6b98369b, "ath9k_hw_releasetxqueue" },
	{ 0x6028e365, "ath9k_hw_reset_tsf" },
	{ 0xd1372381, "wiphy_rfkill_start_polling" },
	{ 0x7d11c268, "jiffies" },
	{ 0xbe288a9c, "skb_trim" },
	{ 0x1691124b, "ieee80211_stop_queues" },
	{ 0xab997430, "usb_unanchor_urb" },
	{ 0x57880904, "__netdev_alloc_skb" },
	{ 0x76e74ccb, "ieee80211_tx_status" },
	{ 0x25176c89, "ath_printk" },
	{ 0xf432dd3d, "__init_waitqueue_head" },
	{ 0xd0e0febc, "ath9k_hw_setopmode" },
	{ 0x6d0aba34, "wait_for_completion" },
	{ 0x94bb0f94, "ath9k_hw_disable" },
	{ 0xd5f2172f, "del_timer_sync" },
	{ 0xf5d208af, "ath9k_hw_resettxqueue" },
	{ 0x6f9d34f1, "ath9k_hw_gettsf64" },
	{ 0x4927350f, "dev_err" },
	{ 0x8f64aa4, "_raw_spin_unlock_irqrestore" },
	{ 0x37befc70, "jiffies_to_msecs" },
	{ 0x2338c6b3, "usb_deregister" },
	{ 0xe3564ba8, "__mutex_init" },
	{ 0x27e1a049, "printk" },
	{ 0x52329fdd, "ath9k_hw_set_sta_beacon_timers" },
	{ 0xce1cdc1, "ath9k_hw_set_tsfadjust" },
	{ 0x2cf100ea, "ieee80211_wake_queues" },
	{ 0xfaef0ed, "__tasklet_schedule" },
	{ 0x3d8d2be5, "ath9k_hw_btcoex_disable" },
	{ 0xb995d029, "ath9k_hw_getrxfilter" },
	{ 0x196aaa63, "ath9k_hw_ani_monitor" },
	{ 0xb4390f9a, "mcount" },
	{ 0x3837325, "usb_control_msg" },
	{ 0x4e2481d5, "ath_is_world_regd" },
	{ 0x16305289, "warn_slowpath_null" },
	{ 0xaaf53e33, "skb_push" },
	{ 0x47b834b7, "mutex_lock" },
	{ 0x9545af6d, "tasklet_init" },
	{ 0x8834396c, "mod_timer" },
	{ 0xe3a2364d, "skb_pull" },
	{ 0x24bfdf30, "wiphy_rfkill_stop_polling" },
	{ 0x23ca9da8, "request_firmware_nowait" },
	{ 0x2c8308c4, "ath9k_cmn_update_ichannel" },
	{ 0x31664b7c, "ath9k_hw_write_associd" },
	{ 0xe3508d5e, "ieee80211_queue_delayed_work" },
	{ 0x6c69ba9e, "dev_kfree_skb_any" },
	{ 0xf11543ff, "find_first_zero_bit" },
	{ 0xd2181cef, "ath_reg_notifier_apply" },
	{ 0x69120ed9, "wiphy_to_ieee80211_hw" },
	{ 0x82072614, "tasklet_kill" },
	{ 0xbfe2e49d, "ath9k_hw_init_btcoex_hw" },
	{ 0x38089e54, "ieee80211_stop_tx_ba_cb_irqsafe" },
	{ 0x65bf35df, "skb_queue_tail" },
	{ 0xf0800f69, "ath9k_hw_beaconq_setup" },
	{ 0xc091d5ec, "_dev_info" },
	{ 0x32bf0f80, "usb_submit_urb" },
	{ 0xf70a52c0, "ath9k_hw_name" },
	{ 0x984ba39f, "ath9k_hw_init_global_settings" },
	{ 0x45158fdd, "__alloc_skb" },
	{ 0xa0168188, "usb_get_dev" },
	{ 0x8b68ce9f, "usb_kill_anchored_urbs" },
	{ 0xd2981357, "ath9k_cmn_count_streams" },
	{ 0xf7c13c24, "ath9k_hw_settsf64" },
	{ 0xba63339c, "_raw_spin_unlock_bh" },
	{ 0xf0fdf6cb, "__stack_chk_fail" },
	{ 0xaea245f9, "wiphy_rfkill_set_hw_state" },
	{ 0x3bd1b1f6, "msecs_to_jiffies" },
	{ 0x22e675f8, "usb_bulk_msg" },
	{ 0x5465ce9e, "usb_put_dev" },
	{ 0xa202a8e5, "kmalloc_order_trace" },
	{ 0xa303019f, "kfree_skb" },
	{ 0xadaf5b92, "ath9k_hw_beaconinit" },
	{ 0xe4c75ef7, "ieee80211_find_sta" },
	{ 0x266868eb, "ieee80211_get_buffered_bc" },
	{ 0x9c17d3e2, "ath9k_hw_btcoex_bt_stomp" },
	{ 0x127789bc, "ath9k_hw_setrxabort" },
	{ 0x49cdbaea, "kmem_cache_alloc_trace" },
	{ 0xd52bf1ce, "_raw_spin_lock" },
	{ 0xcf50ce3a, "ath_hw_setbssidmask" },
	{ 0x9327f5ce, "_raw_spin_lock_irqsave" },
	{ 0xad1563be, "ath9k_hw_phy_disable" },
	{ 0xede72d63, "ieee80211_get_hdrlen_from_skb" },
	{ 0xeeb0be77, "ath9k_hw_setpower" },
	{ 0x4e3538ea, "__ieee80211_create_tpt_led_trigger" },
	{ 0xd3109f79, "ieee80211_register_hw" },
	{ 0x1e8a14e6, "led_classdev_unregister" },
	{ 0x5f8f1381, "ath9k_hw_btcoex_set_weight" },
	{ 0x37a0cba, "kfree" },
	{ 0x1b0a74bc, "regulatory_hint" },
	{ 0x69acdf38, "memcpy" },
	{ 0xf587df1e, "ath9k_hw_setmcastfilter" },
	{ 0xc3b00f02, "ieee80211_start_tx_ba_session" },
	{ 0x6df427e5, "ieee80211_alloc_hw" },
	{ 0x609dcd9e, "ath9k_hw_startpcureceive" },
	{ 0x26b9eeaa, "ath9k_hw_setuptxqueue" },
	{ 0xfe04eb92, "usb_register_driver" },
	{ 0xfd22ccc1, "request_firmware" },
	{ 0xa791e48d, "ath9k_hw_reset" },
	{ 0x522cdc0e, "ieee80211_free_hw" },
	{ 0x71c27a48, "skb_dequeue" },
	{ 0x6a7d881d, "usb_ifnum_to_if" },
	{ 0x4b06d2e7, "complete" },
	{ 0x28318305, "snprintf" },
	{ 0xb0e602eb, "memmove" },
	{ 0x4f91a86d, "ath9k_hw_btcoex_init_3wire" },
	{ 0xce91eaf3, "ath_key_config" },
	{ 0xc458ee4f, "skb_put" },
	{ 0x53f6ffbc, "wait_for_completion_timeout" },
	{ 0x6d044c26, "param_ops_uint" },
	{ 0x910a87c3, "ath9k_hw_reset_calvalid" },
	{ 0x585f9550, "dev_get_drvdata" },
	{ 0xabeaef39, "usb_free_urb" },
	{ 0xbcd4dd16, "release_firmware" },
	{ 0x9bbc2cd7, "ieee80211_start_tx_ba_cb_irqsafe" },
	{ 0x15517275, "usb_anchor_urb" },
	{ 0xc8951505, "usb_alloc_urb" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=mac80211,ath9k_hw,compat,ath9k_common,ath,cfg80211";

MODULE_ALIAS("usb:v0CF3p9271d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0CF3p1006d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0846p9030d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v07D1p3A10d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v13D3p3327d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v13D3p3328d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v13D3p3346d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v13D3p3348d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v13D3p3349d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v13D3p3350d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v04CAp4605d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v040Dp3801d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0CF3pB003d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0CF3pB002d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v057Cp8403d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0CF3p7015d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v1668p1200d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0CF3p7010d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0846p9018d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v083ApA704d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0411p017Fd*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v04DAp3904d*dc*dsc*dp*ic*isc*ip*");
MODULE_ALIAS("usb:v0CF3p20FFd*dc*dsc*dp*ic*isc*ip*");

MODULE_INFO(srcversion, "B8B7CA580458FB26A3B78B9");
