#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xa8a0d3ac, "module_layout" },
	{ 0x92295a0d, "kmalloc_caches" },
	{ 0xb43a926b, "backport_dependency_symbol" },
	{ 0x60a13e90, "rcu_barrier" },
	{ 0x633560f4, "devm_kzalloc" },
	{ 0xeae3dfd6, "__const_udelay" },
	{ 0x25176c89, "ath_printk" },
	{ 0xfb578fc5, "memset" },
	{ 0xb4390f9a, "mcount" },
	{ 0x16305289, "warn_slowpath_null" },
	{ 0x19f2b330, "ath_hw_get_listen_time" },
	{ 0x53358ce3, "ath_regd_get_band_ctl" },
	{ 0xf0fdf6cb, "__stack_chk_fail" },
	{ 0xb9249d16, "cpu_possible_mask" },
	{ 0x4895b7c7, "ath_hw_cycle_counters_update" },
	{ 0x49cdbaea, "kmem_cache_alloc_trace" },
	{ 0xcf50ce3a, "ath_hw_setbssidmask" },
	{ 0x37a0cba, "kfree" },
	{ 0x69acdf38, "memcpy" },
	{ 0x4cbbd171, "__bitmap_weight" },
	{ 0x28318305, "snprintf" },
	{ 0x9e7d6bd0, "__udelay" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=compat,ath";


MODULE_INFO(srcversion, "E3D595BD5FF1580B0394E4F");
